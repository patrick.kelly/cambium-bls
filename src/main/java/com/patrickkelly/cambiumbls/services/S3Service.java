package com.patrickkelly.cambiumbls.services;

import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.iot.model.CannedAccessControlList;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

@Service
public class S3Service {

    private S3Client s3;

    private String bucketName = "com.patrickkelly.cambiumbls";

    public S3Service() {
        Region region = Region.US_EAST_1;
        AwsCredentials credentials = AwsBasicCredentials.create("AKIA56VROX2YWJUFEZXW", "KUS6O0/J+x8dMb2e+3JJidfJWd/C/K6Tmwu778RE");
        this.s3 = S3Client.builder().region(region).credentialsProvider(StaticCredentialsProvider.create(credentials)).build();
    }

    public String putStringToS3(String uploadString, String key) {
        this.s3.putObject(PutObjectRequest.builder().contentType("application/json")
                        .bucket(this.bucketName).key(key).acl(ObjectCannedACL.PUBLIC_READ).build(),
                software.amazon.awssdk.core.sync.RequestBody.fromString(uploadString));

        StringBuilder sb = new StringBuilder();
        sb.append("https://s3.amazonaws.com/");
        sb.append(this.bucketName);
        sb.append("/");
        sb.append(key);

        return sb.toString();
    }
}
