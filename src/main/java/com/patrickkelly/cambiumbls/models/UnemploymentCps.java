package com.patrickkelly.cambiumbls.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class UnemploymentCps {

    @NotNull
    @NotBlank
    private String year;

    private String series_id;
    private String period;
    private String value;
    private String footnote_codes;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private String series_title;

    public String getSeries_id() {
        return series_id;
    }

    public String getYear() {
        return year;
    }

    public String getPeriod() {
        return period;
    }

    public String getValue() {
        return value;
    }

    public String getFootnote_codes() {
        return footnote_codes;
    }

    public String getSeries_title() {
        return series_title;
    }

    public void setSeries_id(String series_id) {
        this.series_id = series_id;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setFootnote_codes(String footnote_codes) {
        this.footnote_codes = footnote_codes;
    }


    public void setSeries_title(String series_title) {
        this.series_title = series_title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
