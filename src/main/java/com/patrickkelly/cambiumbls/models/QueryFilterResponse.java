package com.patrickkelly.cambiumbls.models;

import java.util.List;

public class QueryFilterResponse {

    private String resultLink;
    private List<UnemploymentCps> results;

    public String getResultLink() {
        return resultLink;
    }

    public void setResultLink(String resultLink) {
        this.resultLink = resultLink;
    }

    public List<UnemploymentCps> getResults() {
        return results;
    }

    public void setResults(List<UnemploymentCps> results) {
        this.results = results;
    }
}
