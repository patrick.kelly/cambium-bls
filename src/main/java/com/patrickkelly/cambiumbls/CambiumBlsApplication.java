package com.patrickkelly.cambiumbls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CambiumBlsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CambiumBlsApplication.class, args);
    }

}
