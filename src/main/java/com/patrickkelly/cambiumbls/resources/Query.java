package com.patrickkelly.cambiumbls.resources;

import com.google.gson.GsonBuilder;
import com.patrickkelly.cambiumbls.models.QueryFilterResponse;
import com.patrickkelly.cambiumbls.models.UnemploymentCps;
import com.patrickkelly.cambiumbls.services.S3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/query")
public class Query {

    @Autowired
    private S3Service s3Service;

    @PostMapping("/filter")
    public QueryFilterResponse filterQueryResults(@RequestBody @Valid List<UnemploymentCps> unemploymentCps,
                                                  @RequestParam String year) {
        List<UnemploymentCps> results =
                unemploymentCps.stream().filter(row -> year.equals(row.getYear())).collect(Collectors.toList());

        String jsonResults = new GsonBuilder().setDateFormat("yyyy-MM-dd").create().toJson(results);
        String key = UUID.randomUUID().toString() + ".json";

        String link = s3Service.putStringToS3(jsonResults, key);

        QueryFilterResponse response = new QueryFilterResponse();
        response.setResults(results);
        response.setResultLink(link);

        return response;
    }
}
