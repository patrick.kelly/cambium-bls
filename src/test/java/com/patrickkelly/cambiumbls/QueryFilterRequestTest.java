package com.patrickkelly.cambiumbls;

import com.google.gson.GsonBuilder;
import com.patrickkelly.cambiumbls.models.UnemploymentCps;
import com.patrickkelly.cambiumbls.services.S3Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class QueryFilterRequestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private S3Service s3Service;

    @Test
    public void shouldReturn400ForInvalidBody() throws Exception {
        this.mockMvc.perform(post("/query/filter").contentType(MediaType.APPLICATION_JSON)
                .content("{\"something\": \"something\"}")).andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturn400ForMissingYearParam() throws Exception {

        UnemploymentCps cps = new UnemploymentCps();
        cps.setSeries_id("LNS14000000");
        cps.setYear("2016");
        cps.setPeriod("M01");
        cps.setValue("4.9");
        cps.setDate(new Date());
        cps.setSeries_title("(Seas) Unemployment Rate");

        List<UnemploymentCps> cpsList = new ArrayList<>();
        cpsList.add(cps);

        String json = new GsonBuilder().setDateFormat("yyyy-MM-dd").create().toJson(cpsList);

        this.mockMvc.perform(post("/query/filter").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isBadRequest());
    }

    @Test
    public void shouldReturnEmptyResultsWithLink() throws Exception {
        UnemploymentCps cps = new UnemploymentCps();
        cps.setSeries_id("LNS14000000");
        cps.setYear("2016");
        cps.setPeriod("M01");
        cps.setValue("4.9");
        cps.setDate(new Date());
        cps.setSeries_title("(Seas) Unemployment Rate");

        List<UnemploymentCps> cpsList = new ArrayList<>();
        cpsList.add(cps);

        String json = new GsonBuilder().setDateFormat("yyyy-MM-dd").create().toJson(cpsList);

        when(s3Service.putStringToS3(anyString(), anyString())).thenReturn("http://s3link.com/test.json");

        this.mockMvc.perform(post("/query/filter").param("year", "2015").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(0)))
                .andExpect(jsonPath("$.resultLink", is("http://s3link.com/test.json")));
    }

    @Test
    public void shouldReturnOneResultWithLink() throws Exception {
        UnemploymentCps cps1 = new UnemploymentCps();
        cps1.setSeries_id("LNS14000001");
        cps1.setYear("2015");
        cps1.setPeriod("M02");
        cps1.setValue("5.1");
        cps1.setSeries_title("(Seas) Unemployment Rate");

        UnemploymentCps cps2 = new UnemploymentCps();
        cps2.setSeries_id("LNS14000000");
        cps2.setYear("2016");
        cps2.setPeriod("M01");
        cps2.setValue("4.9");
        cps2.setSeries_title("(Seas) Unemployment Rate");

        List<UnemploymentCps> cpsList = new ArrayList<>();
        cpsList.add(cps1);
        cpsList.add(cps2);

        String json = new GsonBuilder().setDateFormat("yyyy-MM-dd").create().toJson(cpsList);

        when(s3Service.putStringToS3(anyString(), anyString())).thenReturn("http://s3link.com/test.json");

        this.mockMvc.perform(post("/query/filter").param("year", "2015").contentType(MediaType.APPLICATION_JSON)
                .content(json)).andExpect(status().isOk())
                .andExpect(jsonPath("$.results", hasSize(1)))
                .andExpect(jsonPath("$.resultLink", is("http://s3link.com/test.json")))
                .andExpect(jsonPath("$.results[0].series_id", is("LNS14000001")))
                .andExpect(jsonPath("$.results[0].year", is("2015")))
                .andExpect(jsonPath("$.results[0].period", is("M02")))
                .andExpect(jsonPath("$.results[0].value", is("5.1")))
                .andExpect(jsonPath("$.results[0].series_title", is("(Seas) Unemployment Rate")));
    }
}
