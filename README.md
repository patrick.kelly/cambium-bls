To build:

Install Maven, if not installed.

```
mvn install
```

This will also automatically run the tests.

To run:

```
mvn spring-boot:run
```

Application will be running on port 8080.

To use:

POST localhost:8080/query/filter?year=2016

`year` is a required parameter to filter BigQuery BLS results

Example Request Body:
```
[{
  "series_id": "LNS14000000",
  "year": "2016",
  "period": "M01",
  "value": "4.9",
  "footnote_codes": null,
  "date": "2016-01-01",
  "series_title": "(Seas) Unemployment Rate"
},
{
  "series_id": "LNS14000000",
  "year": "2016",
  "period": "M02",
  "value": "4.9",
  "footnote_codes": null,
  "date": "2016-02-01",
  "series_title": "(Seas) Unemployment Rate"
}]
```

Filter results are saved in AWS S3.  Response body contains link to S3 results.

```
{
    "resultLink": "https://s3.amazonaws.com/com.patrickkelly.cambiumbls/8f26f3f0-ef8b-461f-b6cc-e39d2137b9a0.json",
    "results": [
        {
            "year": "2016",
            "series_id": "LNS14000000",
            "period": "M01",
            "value": "4.9",
            "footnote_codes": null,
            "date": "2016-01-01",
            "series_title": "(Seas) Unemployment Rate"
        },
        {
            "year": "2016",
            "series_id": "LNS14000000",
            "period": "M02",
            "value": "4.9",
            "footnote_codes": null,
            "date": "2016-02-01",
            "series_title": "(Seas) Unemployment Rate"
        }
    ]
}
```
